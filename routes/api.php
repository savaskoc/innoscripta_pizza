<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api\\')->name('api.')->group(function () {
    Route::prefix('pizzas')->name('pizzas.')->group(function () {
        Route::get('{pizza}', 'PizzaController@show')->name('show');
        Route::get('', 'PizzaController@index')->name('index');
    });
    Route::prefix('carts')->name('carts.')->group(function () {
        Route::get('user', 'CartController@user')->name('user');
        Route::prefix('{cart}')->group(function () {
            Route::get('', 'CartController@show')->name('show');
            Route::post('', 'CartController@store')->name('store');
            Route::put('', 'CartController@update')->name('update');
            Route::delete('', 'CartController@destroy')->name('destroy');
        });
    });
    Route::post('orders', 'OrderController@store')->name('orders.store');
    Route::post('register', 'Auth\\RegisterController@register')->name('register');
    Route::middleware('auth:api')->group(function () {
        Route::get('user', function (Request $request) {
            return $request->user('api');
        })->name('user');
    });
});
