<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->namespace('Admin\\')->name('admin.')->group(function () {
    Auth::routes();

    Route::middleware('auth', 'is_admin')->group(function () {
        Route::post('pizzas/{pizza}/restore', 'PizzaController@restore')->name('pizzas.restore');
        Route::resource('pizzas', 'PizzaController');
        Route::resource('orders', 'OrderController')->only('index', 'show');

        Route::view('', 'admin.home')->name('home');
    });
});

Route::fallback(function () {
    return view('home');
});
