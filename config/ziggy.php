<?php

return [
    'groups' => [
        'admin' => [
            'admin.*',
        ],
        'api' => [
            'api.*',
        ],
    ],
];
