<?php

namespace App\Providers;

use App\Http\ViewComposers\Admin\CurrencyViewComposer;
use App\Services\ExchangeService;
use App\Services\ExchangeService\ExchangeRatesApiService;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootDependencyInjection();
        $this->bootViewComposers();
    }

    protected function bootDependencyInjection()
    {
        $this->app->bind(ExchangeService::class, ExchangeRatesApiService::class);
    }

    protected function bootViewComposers()
    {
        View::composer(
            ['admin.pizzas.form'],
            CurrencyViewComposer::class
        );
    }
}
