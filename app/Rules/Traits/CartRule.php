<?php

namespace App\Rules\Traits;

use App\Cart;

trait CartRule
{
    protected $cart;

    /**
     * Create a new rule instance.
     *
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    protected function query(string $slug)
    {
        return $this->cart->pizzas()->where('slug', $slug);
    }
}
