<?php

namespace App\Rules;

use App\Cart;
use Illuminate\Contracts\Validation\Rule;

class CartNotEmpty implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Cart::findHash($value)->pizzas()->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.cart_not_empty');
    }
}
