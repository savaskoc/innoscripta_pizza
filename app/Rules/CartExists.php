<?php

namespace App\Rules;

use App\Rules\Traits\CartRule;
use Illuminate\Contracts\Validation\Rule;

class CartExists implements Rule
{
    use CartRule;

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->query($value)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.cart_pizza_exists');
    }
}
