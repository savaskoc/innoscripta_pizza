<?php

namespace App\Rules;

use App\Cart;
use Illuminate\Contracts\Validation\Rule;

class CartIdExists implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Cart::hash($value)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.cart_id_exists');
    }
}
