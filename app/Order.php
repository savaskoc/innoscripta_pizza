<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Order extends Model
{
    protected $fillable = [
        'full_name', 'address', 'zip_code', 'city', 'notes', 'cart_data'
    ];

    protected $casts = [
        'cart_data' => 'json'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getHashAttribute()
    {
        return Hashids::connection('alternative')->encode($this->id);
    }

    public function getFullAddressAttribute()
    {
        return sprintf('%s, %s %s', $this->address, $this->zip_code, $this->city);
    }

    public function getTotalAttribute()
    {
        return implode(' - ', $this->cart_data['total']);
    }

    public function getDeliveryAttribute()
    {
        return implode(' - ', $this->cart_data['delivery'] ?? []);
    }

    public function getDataAttribute()
    {
        return ['id' => $this->hash];
    }
}
