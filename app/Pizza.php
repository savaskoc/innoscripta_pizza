<?php

namespace App;

use Akaunting\Money\Money;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use QCod\ImageUp\HasImageUploads;

class Pizza extends Model
{
    use SoftDeletes,
        Sluggable,
        HasImageUploads;

    protected static $imageFields = [
        'image' => [
            'path' => 'pizzas'
        ]
    ];

    protected $fillable = [
        'name', 'description', 'price', 'price_currency'
    ];

    public static function findBySlug(string $slug)
    {
        return static::where('slug', $slug)->firstOrFail();
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'includeTrashed' => true,
                'onUpdate' => true
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getShortDescriptionAttribute(): ?string
    {
        return Str::limit($this->description, 140);
    }

    public function getPriceTextAttribute(): string
    {
        return $this->getPriceMoneyAttribute()->format();
    }

    public function getPriceMoneyAttribute(): Money
    {
        return money($this->price, $this->price_currency);
    }

    public function getImageUrlAttribute(): ?string
    {
        return $this->image ? $this->imageUrl('image') : null;
    }

    public function getDataAttribute(): array
    {
        return [
            'id' => $this->slug,
            'name' => $this->name
        ];
    }
}
