<?php

namespace App\Services\ExchangeService;

use Akaunting\Money\Currency;
use Akaunting\Money\Money;
use App\Services\ExchangeService;
use DateInterval;
use Illuminate\Support\Facades\Cache;

class ExchangeRatesApiService implements ExchangeService
{
    public function convert(Money $money, Currency $to): Money
    {
        $from = $money->getCurrency();
        return $from->equals($to) ? $money : $money->convert($to, $this->getRatio($from, $to));
    }

    protected function getRatio(Currency $from, Currency $to): float
    {
        $fromKey = $from->getCurrency();
        $toKey = $to->getCurrency();

        $key = $fromKey . $toKey;
        return Cache::get($key, function () use ($key, $fromKey, $toKey) {
            $data = file_get_contents('https://api.exchangeratesapi.io/latest?base=' . $fromKey);
            if ($data === false) {
                return null;
            }
            $ratio = json_decode($data, true)['rates'][$toKey];

            Cache::put($key, $ratio, new DateInterval('PT1H'));
            return $ratio;
        });
    }
}
