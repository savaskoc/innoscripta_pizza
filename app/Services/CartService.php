<?php

namespace App\Services;

use Akaunting\Money\Money;
use App\Cart;
use App\Pizza;
use Vinkla\Hashids\HashidsManager;

class CartService
{
    protected $exchangeService;
    protected $hashids;

    protected $deliveryCosts;
    protected $currencies;

    public function __construct(ExchangeService $exchangeService, HashidsManager $hashids)
    {
        $this->exchangeService = $exchangeService;
        $this->hashids = $hashids;

        $this->currencies = array_keys(config('app.currencies'));
        $this->deliveryCosts = money(config('app.delivery_costs'), config('app.delivery_costs_currency'));
    }

    public function makeData(Cart $cart)
    {
        $pizzas = [];
        foreach ($cart->pizzas as $pizza) {
            /** @var Pizza $pizza */
            $quantity = $pizza->pivot->quantity;
            $prices = [];
            foreach ($this->currencies as $currency) {
                $price = $this->exchangeService->convert($pizza->priceMoney, currency($currency))->multiply($quantity);
                $prices[$currency] = $price;
            }

            $pizzas[] = [
                'id' => $pizza->slug,
                'name' => $pizza->name,
                'quantity' => $quantity,
                'price' => $prices
            ];
        }

        $delivery = [];
        $total = [];
        foreach ($this->currencies as $currency) {
            $currencyObj = currency($currency);

            $deliveryPrice = $this->exchangeService->convert($this->deliveryCosts, $currencyObj);
            $delivery[$currency] = $deliveryPrice->format();

            $total[$currency] = array_reduce($pizzas, function ($acc, &$pizza) use ($currency, $deliveryPrice) {
                return $acc->add($pizza['price'][$currency]);
            }, new Money(0, $currencyObj))->add($deliveryPrice)->format();
        }

        foreach ($pizzas as &$pizza) {
            $pizza['price'] = array_map(function ($price) {
                return $price->format();
            }, $pizza['price']);
        }

        $id = $this->hashids->encode($cart->id);
        return compact('id', 'pizzas', 'delivery', 'total');
    }
}
