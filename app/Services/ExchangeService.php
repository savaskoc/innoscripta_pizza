<?php

namespace App\Services;

use Akaunting\Money\Currency;
use Akaunting\Money\Money;

interface ExchangeService
{
    public function convert(Money $money, Currency $to): Money;
}
