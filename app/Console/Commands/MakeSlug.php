<?php

namespace App\Console\Commands;

use App\Pizza;
use Illuminate\Console\Command;

class MakeSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate slugs for all pizzas';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Pizza::chunk(100, function ($chunk) {
            foreach ($chunk as $pizza) {
                $pizza->name = $pizza->name; //Force regenerate
                $pizza->save();
            }
        });
    }
}
