<?php

namespace App;

use App\Services\CartService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Cart extends Model
{
    public static function findHash($hash): ?Cart
    {
        return Cart::hash($hash)->first();
    }

    public function scopeHash(Builder $builder, $hash)
    {
        $ids = Hashids::decode($hash);
        return $builder->where('id', $ids ? $ids[0] : null);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pizzas()
    {
        return $this->belongsToMany(Pizza::class)->withTimestamps()->withPivot('quantity');
    }

    public function getDataAttribute()
    {
        return resolve(CartService::class)->makeData($this);
    }

    public function resolveRouteBinding($value)
    {
        if (is_numeric($value)) {
            abort(400);
        }
        return static::findHash($value);
    }
}
