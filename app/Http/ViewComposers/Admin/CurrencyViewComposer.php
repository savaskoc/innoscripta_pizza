<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;

class CurrencyViewComposer
{
    public function compose(View $view)
    {
        $view->with('currencies', config('app.currencies'));
    }
}
