<?php

namespace App\Http\Requests\Api;

use App\Cart;
use App\Rules\CartIdExists;
use App\Rules\CartNotEmpty;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cart_id' => [
                'bail',
                'required',
                new CartIdExists,
                new CartNotEmpty
            ],
            'full_name' => 'required|max:255',
            'address' => 'required|max:255',
            'zip_code' => 'required|max:255',
            'city' => 'required|max:255',
            'notes' => 'nullable'
        ];
    }

    protected function passedValidation()
    {
        $this->merge(['cart' => Cart::findHash($this->cart_id)]);
    }
}
