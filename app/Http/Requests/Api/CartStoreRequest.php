<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\Traits\CartRequest;
use App\Rules\CartDoesntExist;
use Illuminate\Foundation\Http\FormRequest;

class CartStoreRequest extends FormRequest
{
    use CartRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->getRulesWithQuantity(new CartDoesntExist($this->cart));
    }
}
