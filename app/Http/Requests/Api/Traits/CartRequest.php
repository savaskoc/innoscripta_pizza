<?php

namespace App\Http\Requests\Api\Traits;

use App\Pizza;
use Illuminate\Validation\Rule;

trait CartRequest
{
    public function passedValidation()
    {
        $this->merge(['pizza' => Pizza::findBySlug($this->pizza_id)]);
    }

    protected function getRules($rule): array
    {
        return [
            'pizza_id' => [
                'bail',
                'required',
                Rule::exists('pizzas', 'slug')->whereNull('deleted_at'),
                $rule
            ]
        ];
    }

    protected function getRulesWithQuantity($rule)
    {
        $rules = $this->getRules($rule);
        $rules['quantity'] = 'required|integer|min:1';
        return $rules;
    }
}
