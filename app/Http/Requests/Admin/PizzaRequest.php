<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PizzaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255', Rule::unique('pizzas')->ignore($this->pizza)],
            'description' => 'nullable',
            'image' => 'nullable|image|max:2048',
            'price' => 'required|integer',
            'price_currency' => ['required', Rule::in(array_keys(config('app.currencies')))]
        ];
    }
}
