<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CartDestroyRequest;
use App\Http\Requests\Api\CartStoreRequest;
use App\Http\Requests\Api\CartUpdateRequest;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function user(Request $request)
    {
        $user = $request->user('api');
        return $this->returnCart($user ? $user->cart : Cart::findHash($request->id) ?? Cart::create());
    }

    public function show(Cart $cart)
    {
        return $this->returnCart($cart);
    }

    public function store(CartStoreRequest $request, Cart $cart)
    {
        $cart->pizzas()->attach($request->pizza, ['quantity' => $request->quantity]);
        return $this->returnCart($cart);
    }

    public function update(CartUpdateRequest $request, Cart $cart)
    {
        $cart->pizzas()->updateExistingPivot($request->pizza, ['quantity' => $request->quantity]);
        return $this->returnCart($cart);
    }

    public function destroy(CartDestroyRequest $request, Cart $cart)
    {
        $cart->pizzas()->detach($request->pizza);
        return $this->returnCart($cart);
    }

    protected function returnCart(Cart $cart)
    {
        return $cart->data;
    }
}
