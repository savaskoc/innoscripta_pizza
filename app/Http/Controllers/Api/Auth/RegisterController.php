<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Admin\Auth\RegisterController as AdminRegisterController;
use Illuminate\Http\Request;

class RegisterController extends AdminRegisterController
{
    protected function registered(Request $request, $user)
    {
        return $user;
    }
}
