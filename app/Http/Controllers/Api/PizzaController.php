<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Pizza;

class PizzaController extends Controller
{
    public function index()
    {
        return Pizza::all()->map->data;
    }

    public function show(Pizza $pizza)
    {
        return $pizza->only('name', 'description', 'price_text', 'image_url');
    }
}
