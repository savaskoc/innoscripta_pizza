<?php

namespace App\Http\Controllers\Api;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\OrderRequest;
use App\Order;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function store(OrderRequest $request)
    {
        $data = DB::transaction(function () use ($request) {
            $order = new Order($request->validated());
            /** @var Cart $cart */
            $cart = $request->cart;

            $order->cart_data = $cart->data;
            if ($user = $request->user('api')) {
                $order->user()->associate($user);
            }
            $order->save();

            $cart->pizzas()->detach();
            $cart->load('pizzas');

            return ['order' => $order->data, 'cart' => $cart->data];
        });
        return $data;
    }
}
