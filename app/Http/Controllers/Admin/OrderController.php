<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    protected $name;
    protected $pluralName;

    public function __construct()
    {
        $this->name = trans_choice('admin.models.order', 1);
        $this->pluralName = trans_choice('admin.models.order', 2);
    }

    public function index()
    {
        $orders = Order::paginate(static::PAGINATION_SIZE);
        $title = $this->pluralName;
        return view('admin.orders.index', compact('title', 'orders'));
    }

    public function show(Order $order)
    {
        $title = sprintf('%s: %s', $this->name, $order->hash);
        return view('admin.orders.show', compact('title', 'order'));
    }
}
