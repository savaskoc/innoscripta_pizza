<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PizzaRequest;
use App\Pizza;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PizzaController extends Controller
{
    protected $name;
    protected $pluralName;

    public function __construct()
    {
        $this->name = trans_choice('admin.models.pizza', 1);
        $this->pluralName = trans_choice('admin.models.pizza', 2);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pizzas = Pizza::withTrashed()->paginate(static::PAGINATION_SIZE);
        $title = $this->pluralName;
        return view('admin.pizzas.index', compact('title', 'pizzas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $title = trans('admin.actions.create', ['name' => $this->name]);
        return $this->showForm($title, new Pizza);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PizzaRequest $request
     * @return Response
     */
    public function store(PizzaRequest $request)
    {
        return $this->update($request, new Pizza);
    }

    /**
     * Display the specified resource.
     *
     * @param Pizza $pizza
     * @return Response
     */
    public function show(Pizza $pizza)
    {
        $title = sprintf('%s: %s', $this->name, $pizza->name);
        return view('admin.pizzas.show', compact('title', 'pizza'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Pizza $pizza
     * @return Response
     */
    public function edit(Pizza $pizza)
    {
        $title = trans('admin.actions.edit', ['name' => $this->name]);
        return $this->showForm($title, $pizza);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PizzaRequest $request
     * @param Pizza $pizza
     * @return Response
     */
    public function update(PizzaRequest $request, Pizza $pizza)
    {
        $pizza->fill($request->validated())->saveOrFail();
        return $this->redirectIndex();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Pizza $pizza
     * @return Response
     */
    public function destroy(Pizza $pizza)
    {
        $pizza->delete();
        return $this->redirectIndex();
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param Pizza $pizza
     * @return Response
     */
    public function restore($pizza)
    {
        Pizza::withTrashed()->firstWhere('slug', $pizza)->restore();
        return $this->redirectIndex();
    }

    protected function showForm(string $title, Pizza $pizza): View
    {
        return view('admin.pizzas.form', compact('title', 'pizza'));
    }

    protected function redirectIndex()
    {
        flash(trans('admin.prompts.success'))->success();
        return redirect()->route('admin.pizzas.index');
    }
}
