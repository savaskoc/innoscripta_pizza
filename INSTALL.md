 1. Clone Git Repository 
`git clone https://bitbucket.org/savaskoc/innoscripta_pizza.git laravel`
 2. Install Composer & Npm Dependencies
`cd laravel && composer install --dev && npm install`
 3. Link storage
`php artisan storage:link`
 4. Copy .env file & edit  within your requirements
`cp .env.example .env && php artisan key:generate` 
 5. Run migrations & seeds
`php artisan migrate --seed`
 6. Deploy laravel passport keys
`php artisan passport:keys`
 7. Generate client key / secret & write them into .env
`php artisan passport:client --password`
You need key and secret into .env as MIX_CLIENT_ID and MIX_CLIENT_SECRET
 8. Run adminlte installation
`php artisan adminlte:install`
 9. Generate routes for frontend
 `php artisan ziggy:generate --group=api resources/js/frontend/route/route.js` 
 11. Run laravel mix
`npm run prod`

You can login into admin panel on /admin
Username: savaskoc11@gmail.com
Password: 12345678
