<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Savas Koc',
            'email' => 'savaskoc11@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('12345678'),
            'is_admin' => true
        ]);
    }
}
