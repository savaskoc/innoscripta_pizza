<?php

/** @var Factory $factory */

use App\Pizza;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Pizza::class, function (Faker $faker) {
    return [
        'name' => $faker->domainWord,
        'description' => $faker->boolean ? $faker->paragraph : null,
        'price' => $faker->numberBetween(100, 3000),
        'price_currency' => $faker->randomElement(['EUR', 'USD', 'TRY']),
    ];
});
