<tr>
    <td>{{ $pizza->name }}</td>
    <td>{{ $pizza->short_description }}</td>
    <td>{{ $pizza->price_text }}</td>
    <td class="text-center">
        <i class="fas fa-fw {{ $pizza->deleted_at ? 'fa-check text-success' : 'fa-times text-danger' }}"></i>
    </td>
    <td class="text-center">
        @if($pizza->deleted_at)
            <a href="{{ route('admin.pizzas.restore', $pizza) }}" class="btn btn-warning btn-xs ml-2"
               data-restore="@lang('admin.prompts.restore')">
                <i class="fas fa-fw fa-redo mr-1"></i>
                @lang('admin.actions.restore', ['name' => ''])
            </a>
        @else
            <a href="{{ route('admin.pizzas.show', $pizza) }}" class="btn btn-info btn-xs">
                <i class="fas fa-fw fa-eye mr-1"></i>
                @lang('admin.actions.show', ['name' => ''])
            </a>
            <a href="{{ route('admin.pizzas.edit', $pizza) }}" class="btn btn-primary btn-xs ml-2">
                <i class="fas fa-fw fa-edit mr-1"></i>
                @lang('admin.actions.edit', ['name' => ''])
            </a>
            <a href="{{ route('admin.pizzas.destroy', $pizza) }}" class="btn btn-danger btn-xs ml-2"
               data-destroy="@lang('admin.prompts.delete')">
                <i class="fas fa-fw fa-trash mr-1"></i>
                @lang('admin.actions.delete', ['name' => ''])
            </a>
        @endif
    </td>
</tr>
