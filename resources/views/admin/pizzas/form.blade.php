@extends('adminlte::page')

@section('plugins.CustomFileInput', true)

@section('title', $title)

@section('content_header')
    <div class="d-flex align-items-center">
        <h1 class="m-0 text-dark">{{ $title }}</h1>
        <a href="{{ route('admin.pizzas.index') }}" class="btn btn-warning btn-sm ml-auto">
            <i class="fas fa-fw fa-chevron-left mr-1"></i>
            @lang('admin.actions.back')
        </a>
        @if($pizza->id)
            <a href="{{ route('admin.pizzas.show', $pizza) }}" class="btn btn-info btn-sm ml-2">
                <i class="fas fa-fw fa-eye mr-1"></i>
                @lang('admin.actions.show', ['name' => $pizza->name])
            </a>
            <a href="{{ route('admin.pizzas.destroy', $pizza) }}" class="btn btn-danger btn-sm ml-2"
               data-destroy="@lang('admin.prompts.delete')">
                <i class="fas fa-fw fa-trash mr-1"></i>
                @lang('admin.actions.delete', ['name' => $pizza->name])
            </a>
        @endif
    </div>
@stop

@section('content')
    @component('admin.components.form')
        @slot('open')
            @if($pizza->id)
                {!! Form::open()->route('admin.pizzas.update', [$pizza])->method('put')->multipart()->fill($pizza) !!}
            @else
                {!! Form::open()->route('admin.pizzas.store')->multipart()->fill($pizza) !!}
            @endif
        @endslot
        <div class="row">
            <div class="col">
                {!! Form::text('name', trans('admin.fields.name')) !!}
            </div>
            <div class="col-md-5 col-xl-3">
                <div class="row">
                    <div class="col">
                        {!! Form::text('price', trans('admin.fields.price')) !!}
                    </div>
                    <div class="col-4">
                        {!! Form::select('price_currency', trans('admin.fields.currency'), $currencies) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::textarea('description', trans('admin.fields.description'))->attrs(['rows' => 5]) !!}
        {!! Form::file('image', trans('admin.fields.image'))->attrs(['data-file' => trans('admin.actions.choose', ['name' => trans('admin.fields.image')])]) !!}
        <div data-preview="#inp-image">
            @if($pizza->image)
                <img src="{{ $pizza->imageUrl('image') }}" class="img-thumbnail" alt="preview">
            @endif
        </div>
    @endcomponent
@stop
