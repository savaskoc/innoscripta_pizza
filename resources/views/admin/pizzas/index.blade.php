@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <div class="d-flex align-items-center">
        <h1 class="m-0 text-dark">{{ $title }}</h1>
        <a href="{{ route('admin.pizzas.create') }}" class="btn btn-success btn-sm ml-auto">
            <i class="fas fa-fw fa-plus mr-1"></i>
            @lang('admin.actions.create', ['name' => trans_choice('admin.models.pizza', 1)])
        </a>
    </div>
@stop

@section('content')
    @component('admin.components.table', ['rows' => $pizzas])
        @slot('thead')
            <tr>
                <th>@lang('admin.fields.name')</th>
                <th>@lang('admin.fields.description')</th>
                <th>@lang('admin.fields.price')</th>
                <th>@lang('admin.fields.deleted')</th>
                <th class="text-center"><i class="fas fa-fw fa-cog"></i></th>
            </tr>
        @endslot
        @each('admin.pizzas.row', $pizzas, 'pizza', 'admin.pizzas.empty')
    @endcomponent
@stop
