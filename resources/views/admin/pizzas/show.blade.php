@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <div class="d-flex align-items-center">
        <h1 class="m-0 text-dark">{{ $title }}</h1>
        <a href="{{ route('admin.pizzas.index') }}" class="btn btn-warning btn-sm ml-auto">
            <i class="fas fa-fw fa-chevron-left mr-1"></i>
            @lang('admin.actions.back')
        </a>
        <a href="{{ route('admin.pizzas.edit', $pizza) }}" class="btn btn-primary btn-sm ml-2">
            <i class="fas fa-fw fa-edit mr-1"></i>
            @lang('admin.actions.edit', ['name' => $pizza->name])
        </a>
        <a href="{{ route('admin.pizzas.destroy', $pizza) }}" class="btn btn-danger btn-sm ml-2"
           data-destroy="@lang('admin.prompts.delete')">
            <i class="fas fa-fw fa-trash mr-1"></i>
            @lang('admin.actions.delete', ['name' => $pizza->name])
        </a>
    </div>
@stop

@section('content')
    <div class="card pizza-detail">
        <div class="card-body">
            @if($pizza->image)
                <img src="{{ $pizza->imageUrl('image') }}" class="float-left" alt="{{ $pizza->name }}">
            @endif
            <h5>{{ $pizza->name }}</h5>
            <p class="m-0">{{ $pizza->description ?? trans('admin.prompts.no_description') }}</p>
        </div>
    </div>
@stop
