@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <div class="d-flex align-items-center">
        <h1 class="m-0 text-dark">{{ $title }}</h1>
    </div>
@stop

@section('content')
    @component('admin.components.table', ['rows' => $orders])
        @slot('thead')
            <tr>
                <th>@lang('admin.fields.order_id')</th>
                <th>@lang('admin.fields.full_name')</th>
                <th>@lang('admin.fields.address')</th>
                <th>@lang('admin.fields.total')</th>
                <th class="text-center"><i class="fas fa-fw fa-cog"></i></th>
            </tr>
        @endslot
        @each('admin.orders.row', $orders, 'order', 'admin.orders.empty')
    @endcomponent
@stop
