<tr>
    <td>{{ $order->hash }}</td>
    <td>{{ $order->full_name }}</td>
    <td>{{ $order->full_address }}</td>
    <td>{{ $order->total }}</td>
    <td class="text-center">
        <a href="{{ route('admin.orders.show', $order) }}" class="btn btn-info btn-xs">
            <i class="fas fa-fw fa-eye mr-1"></i>
            @lang('admin.actions.show', ['name' => ''])
        </a>
    </td>
</tr>
