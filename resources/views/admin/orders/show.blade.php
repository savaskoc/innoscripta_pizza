@extends('adminlte::page')

@section('title', $title)

@section('content_header')
    <div class="d-flex align-items-center">
        <h1 class="m-0 text-dark">{{ $title }}</h1>
        <a href="{{ route('admin.orders.index') }}" class="btn btn-warning btn-sm ml-auto">
            <i class="fas fa-fw fa-chevron-left mr-1"></i>
            @lang('admin.actions.back')
        </a>
    </div>
@stop

@section('content')
    <div class="card">
        <div class="card-header table-responsive p-0">
            <table class="table table-striped table-bordered table-head-fixed text-nowrap m-0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans_choice('admin.models.pizza', 1) }}</th>
                    <th>@lang('admin.fields.quantity')</th>
                    <th>@lang('admin.fields.total')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->cart_data['pizzas'] as $i => $pizza)
                    <tr>
                        <td>{{ $i + 1 }}</td>
                        <td>{{ $pizza['name'] }}</td>
                        <td>{{ $pizza['quantity'] }}</td>
                        <td>{{ implode(' - ', $pizza['price']) }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="3" class="text-right">@lang('admin.fields.delivery')</th>
                    <th>{{ $order->delivery }}</th>
                </tr>
                <tr>
                    <th colspan="3" class="text-right">@lang('admin.fields.total')</th>
                    <th>{{ $order->total }}</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop
