{{ $open }}
<div class="card">
    <div class="card-body">{{ $slot }}</div>
    <div class="card-footer">
        <button class="btn btn-success btn-sm">
            <i class="fas fa-fw fa-save mr-1"></i>
            @lang('admin.actions.save')
        </button>
    </div>
</div>
{!! Form::close() !!}
