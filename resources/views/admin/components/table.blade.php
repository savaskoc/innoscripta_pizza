<div class="card">
    <div class="card-header table-responsive p-0">
        <table class="table table-striped table-bordered table-head-fixed text-nowrap m-0">
            <thead>{{ $thead }}</thead>
            {{ $slot }}
        </table>
    </div>
    @if($rows->hasPages())
        <div class="card-footer d-flex">
            <div class="ml-auto">{{ $rows->links() }}</div>
        </div>
    @endif
</div>
