<?php

return [
    'models' => [
        'pizza' => 'Pizza|Pizzas',
        'order' => 'Order|Orders'
    ],
    'fields' => [
        'image' => 'Image',
        'name' => 'Name',
        'description' => 'Description',
        'price' => 'Price',
        'currency' => 'Currency',
        'deleted' => 'Is Deleted?',
        'order_id' => 'Order #',
        'full_name' => 'Full Name',
        'address' => 'Address',
        'total' => 'Total',
        'delivery' => 'Delivery'
    ],
    'prompts' => [
        'delete' => 'Are you really want to delete this item?',
        'restore' => 'Are you really want to restore this item?',
        'success' => 'Operation Successful',
        'no_description' => 'There are no description for this item.',
        'empty' => 'There are no :name :('
    ],
    'actions' => [
        'save' => 'Save',
        'create' => 'Create :Name',
        'edit' => 'Edit :Name',
        'delete' => 'Delete :Name',
        'choose' => 'Choose :Name',
        'show' => 'Show :Name',
        'restore' => 'Restore :Name',
        'back' => 'Go Back'
    ]
];
