import React, {useState} from 'react'
import {useTranslation} from 'react-i18next'
import {Link, useHistory} from 'react-router-dom'

import {FormGroup, Label, Input, Button} from 'reactstrap'
import {withErrorCallback} from '../swal'
import {register} from '../api'

export default function Register() {
    const {t} = useTranslation()
    const [user, setUser] = useState({})
    const history = useHistory()

    const getUser = field => user[field] ?? ''
    const handleOrder = ({target: {name, value}}) => setUser({...user, [name]: value})

    const handleRegister = async () => {
        const success = await withErrorCallback(register(user), t('prompts.error'))
        if (success) {
            history.replace('/account')
        }
    }

    return (
        <>
            <h1>{t('user.register')}</h1>
            <FormGroup>
                <Label>{t('user.fields.name')}</Label>
                <Input type="text" name="name" value={getUser('name')} onChange={handleOrder} required/>
            </FormGroup>
            <FormGroup>
                <Label>{t('user.fields.username')}</Label>
                <Input type="email" name="email" value={getUser('email')} onChange={handleOrder} required/>
            </FormGroup>
            <FormGroup>
                <Label>{t('user.fields.password')}</Label>
                <Input type="password" name="password" value={getUser('password')} onChange={handleOrder} required/>
            </FormGroup>
            <FormGroup>
                <Label>{t('user.fields.password_confirmation')}</Label>
                <Input type="password" name="password_confirmation" value={getUser('password_confirmation')}
                       onChange={handleOrder} required/>
            </FormGroup>
            <Button color="success" className="mr-2" onClick={handleRegister}>{t('user.register')}</Button>
            <Link to="/login" className="btn btn-primary">{t('user.login')}</Link>
        </>
    )
}
