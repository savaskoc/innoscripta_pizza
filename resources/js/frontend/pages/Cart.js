import React from 'react'
import {useTranslation} from 'react-i18next'
import {useRecoilState} from 'recoil'
import {Alert} from 'reactstrap'

import {cartState} from '../recoil'
import {CartTable, NavLink} from '../components'

export default function Cart() {
    const {t} = useTranslation()
    const [cart, setCart] = useRecoilState(cartState)

    return (
        <>
            <h1>{t('cart.title')}</h1>
            {cart && cart.pizzas.length ? (
                <>
                    <CartTable cart={cart} setCart={setCart} allowModify/>
                    <NavLink to="/checkout" className="btn btn-success">{t('checkout.action')}</NavLink>
                </>
            ) : <Alert color="warning">{t('cart.header_empty')}</Alert>}
        </>
    )
}
