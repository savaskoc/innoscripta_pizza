import {lazy} from 'react'

export const Cart = lazy(() => import('./Cart' /* webpackChunkName: "js/frontend/pages/Cart" */))
export const Checkout = lazy(() => import('./Checkout' /* webpackChunkName: "js/frontend/pages/Checkout" */))
export const CheckoutSuccess = lazy(() => import('./CheckoutSuccess' /* webpackChunkName: "js/frontend/pages/CheckoutSuccess" */))
export const Home = lazy(() => import('./Home' /* webpackChunkName: "js/frontend/pages/Home" */))
export const LogIn = lazy(() => import('./LogIn' /* webpackChunkName: "js/frontend/pages/LogIn" */))
export const NotFound = lazy(() => import('./NotFound' /* webpackChunkName: "js/frontend/pages/NotFound" */))
export const Account = lazy(() => import('./Account' /* webpackChunkName: "js/frontend/pages/Account" */))
export const Pizza = lazy(() => import('./Pizza' /* webpackChunkName: "js/frontend/pages/Pizza" */))
export const Register = lazy(() => import('./Register' /* webpackChunkName: "js/frontend/pages/Register" */))
