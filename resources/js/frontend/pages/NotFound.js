import React from 'react'
import {useTranslation} from 'react-i18next'

import {Alert} from 'reactstrap'

export default function NotFound() {
    const {t} = useTranslation()
    return (
        <Alert color="info">
            {t('prompts.not_found')}
        </Alert>
    )
}
