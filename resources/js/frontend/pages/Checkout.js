import React, {useState} from 'react'
import {useTranslation} from 'react-i18next'
import {useRecoilState} from 'recoil'
import {useHistory} from 'react-router-dom'
import {Alert, Button, FormGroup, Label, Input, Row, Col} from 'reactstrap'

import {cartState} from '../recoil'
import {withErrorCallback} from '../swal'
import {makeOrder} from '../api'
import {CartTable} from '../components'

export default function Checkout() {
    const {t} = useTranslation()
    const [cart, setCart] = useRecoilState(cartState)
    const [order, setOrder] = useState({})
    const history = useHistory();

    const getOrder = field => order[field] ?? ''
    const handleOrder = ({target: {name, value}}) => setOrder({...order, [name]: value})

    const handleMakeOrder = async () => {
        const data = await withErrorCallback(makeOrder(cart.id, order), t('prompts.error'))
        if (data) {
            setCart(data.cart)
            history.push(`/checkout/${data.order.id}`)
        }
    }

    return (
        <>
            <h1>{t('checkout.title')}</h1>
            {cart && cart.pizzas.length ? (
                <>
                    <CartTable cart={cart} setCart={setCart} allowModify/>
                    <h1>{t('checkout.info')}</h1>
                    <FormGroup>
                        <Label>{t('checkout.fields.full_name')}</Label>
                        <Input name="full_name" value={getOrder('full_name')} onChange={handleOrder} required/>
                    </FormGroup>
                    <Row form>
                        <Col>
                            <FormGroup>
                                <Label>{t('checkout.fields.address')}</Label>
                                <Input name="address" value={getOrder('address')} onChange={handleOrder} required/>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <Row form>
                                <Col>
                                    <FormGroup>
                                        <Label>{t('checkout.fields.zip_code')}</Label>
                                        <Input name="zip_code" value={getOrder('zip_code')} onChange={handleOrder}
                                               required/>
                                    </FormGroup>
                                </Col>
                                <Col>
                                    <FormGroup>
                                        <Label>{t('checkout.fields.city')}</Label>
                                        <Input name="city" value={getOrder('city')} onChange={handleOrder} required/>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <FormGroup>
                        <Label>{t('checkout.fields.notes')}</Label>
                        <Input type="textarea" rows={5} name="notes" value={getOrder('notes')} onChange={handleOrder}/>
                    </FormGroup>
                    <Button onClick={handleMakeOrder} color="success" block>{t('checkout.make_order')}</Button>
                </>
            ) : <Alert color="warning">{t('cart.header_empty')}</Alert>}
        </>
    )
}
