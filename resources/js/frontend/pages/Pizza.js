import React from 'react'
import {useParams} from 'react-router-dom'

import {PizzaDetail} from '../components'

export default function Pizza() {
    const {id} = useParams()
    return (
        <PizzaDetail id={id}/>
    )
}
