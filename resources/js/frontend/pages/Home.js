import React from 'react'
import {useTranslation} from 'react-i18next'

import {Alert} from 'reactstrap'

export default function Home() {
    const {t} = useTranslation()
    return (
        <Alert color="primary">{t('home.welcome')}</Alert>
    )
}
