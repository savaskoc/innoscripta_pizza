import React from 'react'
import {useTranslation} from 'react-i18next'
import {useRecoilValue} from 'recoil'
import {Button} from 'reactstrap'

import {userState} from '../recoil'
import {logout} from '../api';
import {CartTable} from '../components'

export default function Account() {
    const {t} = useTranslation()
    const user = useRecoilValue(userState)

    const handleLogout = () => logout()

    if (user === null) {
        return null;
    }

    const renderOrder = (r, i) => <CartTable key={i} cart={r.cart_data} />
    return (
        <>
            <h1>{t('user.account')}</h1>
            <Button color="danger" onClick={handleLogout} className="mb-3">{t('user.logout')}</Button>
            <h2>{t('orders.title')}</h2>
            {user.orders.map(renderOrder)}
        </>
    )
}
