import React from 'react'
import {useTranslation} from 'react-i18next'
import {useParams} from 'react-router-dom'
import {Alert} from 'reactstrap'

export default function Checkout() {
    const {t} = useTranslation()
    const {id: order} = useParams()

    return (
        <>
            <h1>{t('checkout.title')}</h1>
            <Alert>{t('prompts.order_success', {order})}</Alert>
        </>
    )
}
