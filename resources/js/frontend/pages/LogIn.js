import React, {useState} from 'react'
import {useTranslation} from 'react-i18next'
import {Link, useHistory} from 'react-router-dom'

import {FormGroup, Label, Input, Button} from 'reactstrap'
import {withErrorCallback} from '../swal'
import {tryLogin} from '../api'

export default function Login() {
    const {t} = useTranslation()
    const [user, setUser] = useState({})
    const history = useHistory()

    const getUser = field => user[field] ?? ''
    const handleOrder = ({target: {name, value}}) => setUser({...user, [name]: value})

    const handleLogin = async () => {
        const success = await withErrorCallback(tryLogin(user), t('prompts.error'), t('prompts.login_error'))
        if (success) {
            history.replace('/account')
        }
    }

    return (
        <>
            <h1>{t('user.login')}</h1>
            <FormGroup>
                <Label>{t('user.fields.username')}</Label>
                <Input type="email" name="username" value={getUser('username')} onChange={handleOrder} required/>
            </FormGroup>
            <FormGroup>
                <Label>{t('user.fields.password')}</Label>
                <Input type="password" name="password" value={getUser('password')} onChange={handleOrder} required/>
            </FormGroup>
            <Button color="success" className="mr-2" onClick={handleLogin}>{t('user.login')}</Button>
            <Link to="/register" className="btn btn-primary">{t('user.register')}</Link>
        </>
    )
}
