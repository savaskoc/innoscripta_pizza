export const renderPrices = prices => Object.keys(prices).map(k => prices[k]).join(' - ')
