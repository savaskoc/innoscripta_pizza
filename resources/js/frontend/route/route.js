    var Ziggy = {
        namedRoutes: {"api.pizzas.show":{"uri":"api\/pizzas\/{pizza}","methods":["GET","HEAD"],"domain":null},"api.pizzas.index":{"uri":"api\/pizzas","methods":["GET","HEAD"],"domain":null},"api.carts.user":{"uri":"api\/carts\/user","methods":["GET","HEAD"],"domain":null},"api.carts.show":{"uri":"api\/carts\/{cart}","methods":["GET","HEAD"],"domain":null},"api.carts.store":{"uri":"api\/carts\/{cart}","methods":["POST"],"domain":null},"api.carts.update":{"uri":"api\/carts\/{cart}","methods":["PUT"],"domain":null},"api.carts.destroy":{"uri":"api\/carts\/{cart}","methods":["DELETE"],"domain":null},"api.orders.store":{"uri":"api\/orders","methods":["POST"],"domain":null},"api.register":{"uri":"api\/register","methods":["POST"],"domain":null},"api.user":{"uri":"api\/user","methods":["GET","HEAD"],"domain":null}},
        baseUrl: 'http://localhost:8000/',
        baseProtocol: 'http',
        baseDomain: 'localhost',
        basePort: 8000,
        defaultParameters: []
    };

    if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
        for (var name in window.Ziggy.namedRoutes) {
            Ziggy.namedRoutes[name] = window.Ziggy.namedRoutes[name];
        }
    }

    export {
        Ziggy
    }
