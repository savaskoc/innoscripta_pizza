import {default as z_route} from 'ziggy'
import {Ziggy} from './route'

export default function route(name, params, absolute) {
    return z_route(name, params, absolute, Ziggy)
}
