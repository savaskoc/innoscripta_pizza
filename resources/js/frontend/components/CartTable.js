import React from 'react'
import {useTranslation} from 'react-i18next'
import {Button, Table} from 'reactstrap'

import {destroyCart, updateCart} from '../api'
import {confirm, withError} from '../swal'

import {Counter} from './index'
import {renderPrices} from '../utils'

export default function CartTable({cart: {id: cartId, pizzas, delivery, total}, setCart, allowModify}) {
    const {t} = useTranslation()

    const handleUpdate = async (id, quantity) => {
        const data = await withError(
            updateCart(cartId, id, quantity),
            {title: t('cart.order'), text: t('cart.update_success')},
            t('prompts.error')
        )
        if (data) {
            setCart(data)
        }
    }

    const handleDestroy = async (id) => {
        const value = await confirm(t('cart.order'), t('prompts.destroy'))
        if (value) {
            const data = await withError(
                destroyCart(cartId, id),
                {title: t('cart.order'), text: t('cart.destroy_success')},
                t('prompts.error')
            )
            if (data) {
                setCart(data)
            }
        }
    }

    const renderItem = ({id, name, quantity, price}, i) => (
        <tr key={i}>
            <td>{i + 1}</td>
            <td>{name}</td>
            <td>
                {
                    allowModify
                        ? <Counter count={quantity} onChange={quantity => handleUpdate(id, quantity)}/>
                        : quantity
                }
            </td>
            <td>{renderPrices(price)}</td>
            {allowModify && (
                <td><Button size="sm" color="danger" onClick={() => handleDestroy(id)}>{t('cart.destroy')}</Button></td>
            )}
        </tr>
    )

    return (
        <Table className="cart" bordered striped hover responsive>
            <thead>
            <tr>
                <th width={1}>#</th>
                <th>{t('cart.fields.pizza')}</th>
                <th>{t('cart.fields.quantity')}</th>
                <th>{t('cart.fields.price')}</th>
                {allowModify && <th width={1}/>}
            </tr>
            </thead>
            <tbody>
            {pizzas.map(renderItem)}
            </tbody>
            <tfoot>
            <tr>
                <th colSpan={3} className="text-right">{t('cart.delivery')}</th>
                <th colSpan={allowModify ? 2 : 1}>{renderPrices(delivery)}</th>
            </tr>
            <tr>
                <th colSpan={3} className="text-right">{t('cart.total')}</th>
                <th colSpan={allowModify ? 2 : 1}>{renderPrices(total)}</th>
            </tr>
            </tfoot>
        </Table>
    )
}
