import React from 'react'
import {Col, Row} from 'reactstrap'

import {getPizza} from '../api'
import AddToCart from './AddToCart'

export default function PizzaDetail({id}) {
    const {name, description, price_text, image_url} = getPizza(id)
    return (
        <Row>
            {image_url && (
                <Col md="2">
                    <img src={image_url} alt={name} className="img-fluid mb-3"/>
                </Col>
            )}
            <Col>
                <h1>{name}</h1>
                {description && <p>{description}</p>}
                {price_text}
                <Row>
                    <Col md="3">
                        <AddToCart id={id}/>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}
