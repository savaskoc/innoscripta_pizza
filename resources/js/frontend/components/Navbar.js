import React, {Suspense, useEffect, useState} from 'react'
import {useTranslation} from "react-i18next";
import {useLocation} from 'react-router-dom'
import {Collapse, Container, Navbar as RNavbar, NavbarBrand, NavbarText, NavbarToggler} from 'reactstrap'

import Menu from './Menu'
import Cart from './Cart'
import User from './User'

export default function Navbar() {
    const {t} = useTranslation()
    const [isOpen, setIsOpen] = useState(false)

    const location = useLocation()
    useEffect(() => setIsOpen(false), [location])

    const toggle = () => setIsOpen(!isOpen)
    return (
        <RNavbar expand="md" color="primary" dark>
            <Container>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Suspense fallback={<NavbarText>{t('prompts.loading')}</NavbarText>}>
                        <Menu/>
                    </Suspense>
                    <Suspense fallback={<NavbarText>{t('prompts.loading')}</NavbarText>}>
                        <Cart/>
                        <User/>
                    </Suspense>
                </Collapse>
            </Container>
        </RNavbar>
    )
}
