import React, {useEffect} from 'react'
import {useTranslation} from 'react-i18next'
import {useRecoilState} from 'recoil'

import {getCart} from '../api'
import {cartState} from '../recoil'
import NavLink from './NavLink'
import {renderPrices} from '../utils'

export default function Cart() {
    const {t} = useTranslation()
    const [cart, setCart] = useRecoilState(cartState)

    const remoteCart = getCart()
    useEffect(() => {
        const cartId = localStorage.getItem('cart')
        if (!cartId) {
            localStorage.setItem('cart', remoteCart.id)
        }
        setCart(remoteCart)
    }, [remoteCart])

    if (cart === null) {
        return null
    }

    const {pizzas, total} = cart
    return (
        <NavLink to="/cart" className="cart">
            {
                pizzas.length
                    ? t('cart.header', {count: pizzas.length, total: renderPrices(total)})
                    : t('cart.header_empty')
            }
        </NavLink>
    )
}
