import React from 'react'
import {useTranslation} from 'react-i18next'
import {Nav} from 'reactstrap'

import {getPizzas} from '../api'
import NavLink from './NavLink'

export default function Menu() {
    const {t} = useTranslation()
    const pizzas = getPizzas()
    return (
        <Nav className="mr-auto" navbar>
            <NavLink to="/" exact>Pizza</NavLink>
            {pizzas.map(({id, name}, i) => <NavLink key={i} to={`/pizzas/${id}`}>{name}</NavLink>)}
        </Nav>
    )
}
