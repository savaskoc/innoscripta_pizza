import React from 'react'
import {Button, Input, InputGroup, InputGroupAddon} from 'reactstrap'

export default function Counter({count, onChange, min, ...props}) {
    return (
        <InputGroup {...props}>
            <InputGroupAddon addonType="prepend">
                <Button onClick={() => onChange(count - 1)}>-</Button>
            </InputGroupAddon>
            <Input type="number" value={count} onChange={({target: {value}}) => onChange(+value)} min={min || 1}/>
            <InputGroupAddon addonType="append">
                <Button onClick={() => onChange(count + 1)}>+</Button>
            </InputGroupAddon>
        </InputGroup>
    )
}
