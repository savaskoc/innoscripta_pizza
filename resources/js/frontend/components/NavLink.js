import React from "react";
import {NavLink as RNavLink} from 'reactstrap'
import {NavLink as RRNavLink} from 'react-router-dom'

export default function NavLink({...props}) {
    return <RNavLink tag={RRNavLink} {...props} />
}
