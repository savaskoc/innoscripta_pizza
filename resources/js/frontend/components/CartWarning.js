import React from 'react'
import {useTranslation} from 'react-i18next'
import {useRecoilValue} from 'recoil'
import {Alert} from 'reactstrap'

import {cartState} from '../recoil'

export default function Cart({id}) {
    const {t} = useTranslation()
    const {pizzas} = useRecoilValue(cartState)

    const inCart = pizzas.filter(p => p.id === id).length > 0
    return inCart && <Alert color="info">{t('cart.exists')}</Alert>
}
