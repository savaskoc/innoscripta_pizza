import React, {useEffect} from 'react'
import {useTranslation} from 'react-i18next'
import {useRecoilState} from 'recoil'

import {userState} from '../recoil'
import {getUser} from '../api'
import NavLink from './NavLink'

export default function User() {
    const {t} = useTranslation()
    const [user, setUser] = useRecoilState(userState)

    const [_, {data}] = getUser()
    useEffect(() => {
        console.log('user', data)
        if (data) {
            setUser(data)
        }
    }, [data])

    return user === null
        ? <NavLink to="/login" className="cart">{t('user.login')}</NavLink>
        : <NavLink to="/account" className="cart">{user.email}</NavLink>
}
