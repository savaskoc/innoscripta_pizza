import React, {useEffect, useState} from 'react'
import {useTranslation} from 'react-i18next'
import {useRecoilState} from 'recoil'
import {Button} from 'reactstrap'

import {cartState} from '../recoil'
import {withError} from '../swal'
import {addCart} from '../api'
import Counter from './Counter'
import CartWarning from './CartWarning'

const minCount = 1

export default function AddToCart({id}) {
    const {t} = useTranslation()
    const [count, setCount] = useState(minCount)
    const [{id: cartId}, setCart] = useRecoilState(cartState)

    const handleCount = num => setCount(Math.max(minCount, num))
    useEffect(() => handleCount(minCount), [id])

    const handleAdd = async () => {
        const data = await withError(
            addCart(cartId, id, count),
            {title: t('cart.order'), text: t('cart.add_success')},
            t('prompts.error')
        )
        if (data) {
            setCart(data)
        }
    }

    return (
        <>
            <h2 className="mt-3">{t('cart.order')}</h2>
            <Counter className="mb-2" count={count} onChange={handleCount} min={minCount}/>
            <CartWarning id={id}/>
            <Button color="success" onClick={handleAdd}>{t("cart.add")}</Button>
        </>
    )
}
