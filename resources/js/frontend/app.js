import React from 'react'
import ReactDOM from 'react-dom'
import {RecoilRoot} from 'recoil'
import {BrowserRouter} from 'react-router-dom'

import Layout from './Layout'

function App() {

    return (
        <RecoilRoot>
            <BrowserRouter>
                <Layout/>
            </BrowserRouter>
        </RecoilRoot>
    )
}

ReactDOM.render(<App/>, document.getElementById('app'))
