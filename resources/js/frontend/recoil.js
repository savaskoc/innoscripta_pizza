import {atom} from 'recoil'

import {getCart} from './api'

export const cartState = atom({
    key: 'cart',
    default: null
})

export const userState = atom({
    key: 'user',
    default: null
})
