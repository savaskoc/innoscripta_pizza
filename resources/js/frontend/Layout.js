import 'bootstrap/dist/css/bootstrap.min.css'
import '../../sass/frontend/app.scss'
import './i18n'

import React, {Suspense, useEffect} from 'react'
import {useTranslation} from 'react-i18next'
import {Alert, Container} from 'reactstrap'
import {Route, Switch} from 'react-router-dom'

import {Navbar} from './components'
import {Cart, Checkout, CheckoutSuccess, Home, LogIn, NotFound, Account, Pizza, Register} from './pages'

export default function Layout() {
    const {t} = useTranslation()

    return (
        <>
            <Navbar/>
            <Container className="my-3">
                <Suspense fallback={<Alert color="info">{t('prompts.loading')}</Alert>}>
                    <Switch>
                        <Route path="/pizzas/:id">
                            <Pizza/>
                        </Route>
                        <Route path="/checkout/:id">
                            <CheckoutSuccess/>
                        </Route>
                        <Route path="/checkout">
                            <Checkout/>
                        </Route>
                        <Route path="/cart">
                            <Cart/>
                        </Route>
                        <Route path="/account">
                            <Account/>
                        </Route>
                        <Route path="/login">
                            <LogIn/>
                        </Route>
                        <Route path="/register">
                            <Register/>
                        </Route>
                        <Route path="/" exact>
                            <Home/>
                        </Route>
                        <Route path="*">
                            <NotFound/>
                        </Route>
                    </Switch>
                </Suspense>
            </Container>
        </>
    )
}
