import {create} from 'use-axios'
import axios from 'axios'
import oauth from 'axios-oauth-client'
import tokenProvider from 'axios-token-interceptor'

import route from './route'

const axiosInstance = axios.create({})
const {useAxios, refetch, useAxiosSafe} = create(axiosInstance)

export function getPizzas() {
    const url = route('api.pizzas.index').url()
    return useAxios(url).data
}

export function getPizza(id) {
    const url = route('api.pizzas.show', id).url()
    return useAxios(url).data
}

const getCartUrl = () => `${route('api.carts.user').url()}?id=${localStorage.getItem('cart')}`
const refetchCart = () => refetch(getCartUrl())

export const getCart = () => useAxios(getCartUrl()).data

const cartAction = (id, routeName, method, data) => {
    const url = route(routeName, id).url()
    return axiosInstance(url, {method, data}).then(({data: response}) => response)
}

export function addCart(id, pizza_id, quantity) {
    return cartAction(id, 'api.carts.store', 'post', {pizza_id, quantity})
}

export function updateCart(id, pizza_id, quantity) {
    return cartAction(id, 'api.carts.update', 'put', {pizza_id, quantity})
}

export function destroyCart(id, pizza_id) {
    return cartAction(id, 'api.carts.destroy', 'delete', {pizza_id})
}

export function makeOrder(cart_id, data) {
    const url = route('api.orders.store').url()
    return axiosInstance.post(url, {cart_id, ...data}).then(({data: response}) => {
        refetchUser()
        return response
    })
}

const getUserUrl = () => route('api.user').url()
const refetchUser = () => refetch(getUserUrl())

export function getUser() {
    return useAxiosSafe(getUserUrl())
}

const login = (token) => {
    axiosInstance.interceptors.request.use(oauth.interceptor(tokenProvider, () => Promise.resolve(token)))
    localStorage.setItem('token', JSON.stringify(token))

    refetchUser()
    refetchCart()
    return true
}

export async function tryLogin({username, password}) {
    const getOwnerCredentials = oauth.client(axiosInstance, {
        url: `${location.origin}/oauth/token`,
        grant_type: 'password',
        client_id: process.env.MIX_CLIENT_ID,
        client_secret: process.env.MIX_CLIENT_SECRET,
        username,
        password
    })
    const token = await getOwnerCredentials()
    return login(token)
}

export function logout() {
    localStorage.removeItem('token')
    location.assign('/')
}

export async function register(data) {
    const url = route('api.register').url()
    await axiosInstance.post(url, data)

    const {email: username, password} = data
    return await tryLogin({username, password})
}

const json = localStorage.getItem('token')
if (json) {
    login(JSON.parse(json))
}
