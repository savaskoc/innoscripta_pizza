import React from 'react'
import swal from '@sweetalert/with-react'

export function withErrorCallback(promise, errorTitle, defaultError = '') {
    return promise.catch(e => {
        let error = defaultError
        const errors = e.response.data.errors || false
        if (errors) {
            error = Object.keys(errors).flatMap(key => errors[key]).join('<br>')
        }

        swal({
            title: errorTitle,
            icon: 'error',
            content: <p dangerouslySetInnerHTML={{__html: error}}/>
        })
    })
}

export function withError(promise, {title, text}, errorTitle, defaultError = '') {
    return withErrorCallback(promise, errorTitle, defaultError).then(data => {
        if (data) {
            swal(title, text, 'success')
        }
        return data
    })
}

export function confirm(title, text) {
    return swal({
        title,
        text,
        icon: 'warning',
        buttons: true,
        dangerMode: true
    })
}
