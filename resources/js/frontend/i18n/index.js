import i18n from 'i18next'
import {initReactI18next} from 'react-i18next'

i18n.use(initReactI18next).init({
    resources: {
        en: {
            translation: require('./en.json')
        }
    },
    lng: document.querySelector('html').lang,
    interpolation: {
        escapeValue: false
    }
});
export default i18n;
