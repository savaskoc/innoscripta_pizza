const csrfToken = $('meta[name="csrf-token"]').attr('content')

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': csrfToken
    }
})

function promptAndSubmit(e, text, method) {
    Swal.fire({
        title: 'Warning',
        text: e.data(text),
        type: 'warning',
        showConfirmButton: true,
        showCancelButton: true
    }).then(({value}) => {
        if (value) {
            const csrfField = $('<input type="hidden" name="_token">').val(csrfToken)
            const methodField = $('<input type="hidden" name="_method">').val(method)
            $('<form method="post" />').attr('action', e.attr('href')).append(csrfField).append(methodField).insertAfter(e).submit()
        }
    })
    return false
}

$('[data-destroy]').click(function () {
    return promptAndSubmit($(this), 'destroy', 'delete')
})

$('[data-restore]').click(function () {
    return promptAndSubmit($(this), 'restore', 'post')
})

$('[data-file]').each(function () {
    const _this = $(this)
    _this.addClass('custom-file-input').wrap('<div class="custom-file" />')
    $('<label class="custom-file-label" />').insertAfter(_this).attr('for', _this.attr('id')).text(_this.data('file'))
})

if (window.hasOwnProperty('bsCustomFileInput')) {
    bsCustomFileInput.init()
}

function imagePreview(preview) {
    return function () {
        if (this.files && this.files[0]) {
            const reader = new FileReader()
            reader.onload = function (e) {
                preview.html($('<img class="img-thumbnail" alt="preview" />').attr('src', e.target.result))
            }
            reader.readAsDataURL(this.files[0])
        }
    }
}

$('[data-preview]').each(function () {
    const _this = $(this)
    $(_this.data('preview')).change(imagePreview(_this))
})
